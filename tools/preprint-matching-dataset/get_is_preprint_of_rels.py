import csv
import requests


with open("data/existing_is_preprint_of.csv", "w") as f:
    writer = csv.writer(f)
    cursor = "*"
    i = 0
    j = 0
    while True:
        print(i, j)
        i += 1
        params = {
            "cursor": cursor,
            "filter": "relation.type:is-preprint-of,until-created-date:2023-08-31",
            "rows": 1000,
        }
        data = requests.get(
            "https://api.crossref.org/types/posted-content/works", params
        ).json()["message"]
        cursor = data["next-cursor"]
        if not data["items"]:
            break
        for work in data["items"]:
            if work.get("subtype", "") != "preprint":
                continue
            for related in work["relation"]["is-preprint-of"]:
                if "doi" != related["id-type"]:
                    continue
                writer.writerow(
                    [
                        "is-preprint-of",
                        related["asserted-by"],
                        related["id"].lower(),
                        work["DOI"],
                    ]
                )
                j += 1
