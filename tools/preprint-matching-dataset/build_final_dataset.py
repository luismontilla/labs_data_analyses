import csv
import json

data = {}

with open("data/existing_has_preprint_cleaned.csv", "r") as f:
    reader = csv.reader(f)
    for row in reader:
        relation, asserted, article_doi, preprint_doi = row
        if (preprint_doi, article_doi) not in data:
            data[(preprint_doi, article_doi)] = {
                "article": False,
                "preprint": False,
                "confidence": "",
            }
        if asserted == "subject":
            data[(preprint_doi, article_doi)]["article"] = True
        else:
            data[(preprint_doi, article_doi)]["preprint"] = True

with open("data/existing_is_preprint_of_cleaned.csv", "r") as f:
    reader = csv.reader(f)
    for row in reader:
        relation, asserted, article_doi, preprint_doi = row
        if (preprint_doi, article_doi) not in data:
            data[(preprint_doi, article_doi)] = {
                "article": False,
                "preprint": False,
                "confidence": "",
            }
        if asserted == "subject":
            data[(preprint_doi, article_doi)]["preprint"] = True
        else:
            data[(preprint_doi, article_doi)]["article"] = True

for fn in ["data/preprint-links.txt"]:
    with open(fn, "r") as f:
        for line in f:
            article_doi, matches = line.strip().split("\t")
            matches = json.loads(matches)
            if matches in ["wrong date", "wrong type"]:
                continue
            for m in matches:
                preprint_doi = m["id"][16:]
                if (preprint_doi, article_doi) not in data:
                    data[(preprint_doi, article_doi)] = {
                        "article": False,
                        "preprint": False,
                        "confidence": 0,
                    }
                data[(preprint_doi, article_doi)]["confidence"] = m["confidence"]

total = 0
pub_only = 0
match_only = 0
both = 0
articles = set()
preprints = set()
with open("data/final-dataset.csv", "w") as f:
    writer = csv.writer(f)
    for preprint_doi, article_doi in data:
        articles.add(article_doi)
        preprints.add(preprint_doi)
        writer.writerow(
            [
                preprint_doi,
                article_doi,
                data[(preprint_doi, article_doi)]["article"],
                data[(preprint_doi, article_doi)]["preprint"],
                data[(preprint_doi, article_doi)]["confidence"],
            ]
        )
        total += 1
        pub = (
            data[(preprint_doi, article_doi)]["article"]
            or data[(preprint_doi, article_doi)]["preprint"]
        )
        match = data[(preprint_doi, article_doi)]["confidence"] != ""
        if pub and match:
            both += 1
        elif pub:
            pub_only += 1
        elif match:
            match_only += 1

print("Total", total)
print("#articles", len(articles))
print("#preprints", len(preprints))
print("Pub only", pub_only, pub_only / total)
print("Match only", match_only, match_only / total)
print("Both", both, both / total)
