import csv
import requests


with open("data/existing_has_preprint.csv", "r") as f, open(
    "data/existing_has_preprint_cleaned.csv", "a"
) as f2:
    reader = csv.reader(f)
    writer = csv.writer(f2)
    for i, row in enumerate(reader):
        print(i)
        k, _, da, dp = row
        ra = requests.get("https://api.crossref.org/works/" + da)
        if ra.status_code == 404:
            continue
        ra = ra.json()["message"]
        if "journal-article" != ra.get("type", ""):
            continue
        created = ra["created"]["date-parts"][0]
        if created[0] == 2023 and created[1] > 8:
            continue
        rp = requests.get("https://api.crossref.org/works/" + dp)
        if rp.status_code == 404:
            continue
        rp = rp.json()["message"]
        if "posted-content" != rp.get("type", ""):
            continue
        if "preprint" != rp.get("subtype", ""):
            continue
        created = rp["created"]["date-parts"][0]
        if created[0] == 2023 and created[1] > 8:
            continue
        writer.writerow(row)
