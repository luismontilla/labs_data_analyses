import csv
import requests


with open("data/existing_has_preprint.csv", "w") as f:
    writer = csv.writer(f)
    cursor = "*"
    i = 0
    j = 0
    while True:
        print(i, j)
        i += 1
        params = {
            "cursor": cursor,
            "filter": "relation.type:has-preprint,until-created-date:2023-08-31",
            "rows": 1000,
        }
        data = requests.get(
            "https://api.crossref.org/types/journal-article/works", params
        ).json()["message"]
        cursor = data["next-cursor"]
        if not data["items"]:
            break
        for work in data["items"]:
            for related in work["relation"]["has-preprint"]:
                if "doi" != related["id-type"]:
                    continue
                writer.writerow(
                    [
                        "has-preprint",
                        related["asserted-by"],
                        work["DOI"],
                        related["id"].lower(),
                    ]
                )
                j += 1
