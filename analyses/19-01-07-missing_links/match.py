import argparse
import logging
import matching.openurl_query_matcher
import matching.stq_matcher
import re
import sys
import utils.data_format_keys as dfk

from multiprocessing import Pool
from random import sample
from utils.utils import init_logging, read_json, save_json


def extract_references(sample_works):
    references = []
    for work in sample_works.get(dfk.SAMPLE_SAMPLE):
        source_doi = work.get(dfk.CR_ITEM_DOI)
        for ref in work.get(dfk.CR_ITEM_REFERENCE, []):
            ref['source_DOI'] = source_doi
            references.append(ref)
    return references


def match_openurl(references):
    matcher = matching.openurl_query_matcher.Matcher()
    input_refs = [r for r in references if dfk.CR_ITEM_DOI not in r]
    logging.info('Input references: {}'.format(len(input_refs)))
    with Pool(5) as p:
        results = p.map(matcher.match, input_refs)
    [item.update({'DOI_from_structured': None}) for item in references]
    for item, doi in zip(input_refs, results):
        item['DOI_from_structured'] = doi[0]


def match_stq(references):
    matcher = matching.stq_matcher.Matcher()
    input_refs = [r for r in references if dfk.CR_ITEM_DOI not in r
                  and dfk.CR_ITEM_UNSTRUCTURED in r]
    logging.info('Input references: {}'.format(len(input_refs)))
    with Pool(5) as p:
        results = p.map(matcher.match,
                        [re.sub('\n', ' ', r[dfk.CR_ITEM_UNSTRUCTURED])
                         for r in input_refs])
    [item.update({'DOI_from_unstructured': None}) for item in references]
    for item, doi in zip(input_refs, results):
        item['DOI_from_unstructured'] = doi[0]


if __name__ == '__main__':
    sys.setrecursionlimit(10000)

    parser = argparse.ArgumentParser(
        description='match references from a sample')
    parser.add_argument('-v', '--verbose', help='verbose output',
                        action='store_true')
    parser.add_argument('-s', '--sample', type=str, required=True)
    parser.add_argument('-n', '--references', type=int)
    parser.add_argument('-o', '--output', type=str, required=True)

    args = parser.parse_args()

    init_logging(args.verbose)

    sample_data = read_json(args.sample)

    references = extract_references(sample_data)
    logging.info('Total number of references: {}'.format(len(references)))

    if args.references is not None:
        references = sample(references, args.references)

    logging.info('Sampled number of references: {}'.format(len(references)))

    logging.info('Matching with OpenURL')
    match_openurl(references)

    logging.info('Matching with SimpleTextQuery')
    match_stq(references)

    save_json(references, args.output)
