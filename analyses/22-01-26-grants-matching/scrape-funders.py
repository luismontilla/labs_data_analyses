import json
import random
import re
import requests
import time
import sys

from multiprocessing import Pool

mailto = '<EMAIL>'
url = 'https://api.crossref.org/'

def iterate_items(path, filters=None):
    cursor = '*'
    while True:
        params = {'mailto': mailto, 'cursor': cursor, 'rows': 500}
        if filters:
            params['filter'] = filters
        data = requests.get(url + path, params).json()['message']
        items = data['items']
        cursor = data['next-cursor']
        if not items:
            break
        for item in items:
            yield item

def funder_doi_norm(doi):
    if not doi:
        return ''
    return '10.13039/' + re.sub('.*\/', '', doi)

def funder_aliases(funder):
    aliases = funder['replaces'] + funder['replaced-by']
    return list(set([funder_doi_norm(a) for a in aliases]))

def build_funder(funder):
    time.sleep(random.random())
    try:
        response = requests.get(url + 'funders/' + funder, {'mailto': mailto})
    except:
        time.sleep(10)
        try:
            response = requests.get(url + 'funders/' + funder, {'mailto': mailto})
        except:
            time.sleep(10)
            response = requests.get(url + 'funders/' + funder, {'mailto': mailto})
    d = response.json()
    data = d['message']
    return {'DOI': funder_doi_norm(data['id']),
            'aliases': funder_aliases(data),
            'names': [s.lower() for s in [data['name']] + data['alt-names']],
            'descendants': [funder_doi_norm(d) for d in data['descendants']]}

if __name__ == '__main__':

    funders = []
    for i, funder in enumerate(iterate_items('funders')):
        print(i)
        funders.append(funder_doi_norm(funder['id']))

    funders.sort()

    with Pool(4) as pool:
        results = pool.imap(build_funder, funders)
    
        with open('data/funders-data-1.jsonl', 'a') as f:
            for i, funder in enumerate(results):
                print(i)
                f.write(json.dumps(funder) + '\n')
