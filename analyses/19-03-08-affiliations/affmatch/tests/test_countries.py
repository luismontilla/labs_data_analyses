from affmatch.countries import load_countries, to_region, get_country_codes, \
    get_countries


def test_load_countries():
    countries = load_countries()
    assert len(countries) == 588
    assert ('az', 'azarbaycan respublikasi') in countries
    assert ('fm', 'federated states of micronesia') in countries
    assert ('zm', 'zambia') in countries


def test_to_region():
    assert to_region('PL') == 'PL'
    for c in ['GB', 'UK']:
        assert to_region(c) == 'GB-UK'
    if c in ['CN', 'HK', 'TW']:
        assert to_region(c) == 'CN-HK-TW'
    if c in ['PR', 'US']:
        assert to_region(c) == 'US-PR'


def test_get_country_codes():
    assert get_country_codes('Seoul, Korea.') == ['KR']
    assert get_country_codes('Chicago, Illinois, USA') == ['US']
    assert \
        get_country_codes('University of California, Berkeley, California') \
        == ['US']
    assert get_country_codes('Hospital Kassel, Kassel, Germany') == ['DE']
    assert get_country_codes('New South Wales, Australia') == ['AU']
    assert get_country_codes('State of Illinois') == ['US']
    assert get_country_codes('Lehigh Valley Hospital, Allentown, PA;') == \
        ['US']
    assert get_country_codes('Boston Children\'s Hospital, Boston, MA ') == \
        ['US']
    assert get_country_codes('Winthrop University Hospital, Mineola, NY') == \
        ['US']
    assert get_country_codes('Medical Dow Chemical Company, U.S.A.') == ['US']
    assert get_country_codes('New York University') == ['US']
    assert get_country_codes('Enschede, The Netherlands') == ['NL']
    assert \
        get_country_codes('University of Surrey, Guildford, United Kingdom') \
        == ['UK']
    assert get_country_codes('República Dominicana') == ['DO']
    assert get_country_codes('České Budějovice ,  Czech Republic') == ['CZ']
    assert get_country_codes('Washington, D.C.') == ['US']
    assert get_country_codes('Agency for Health Care Policy and Research') == \
        []


def test_get_country():
    assert get_countries('Seoul, Korea.') == ['KR']
    assert get_countries('Chicago, Illinois, USA') == ['US-PR']
    assert get_countries('University of California, Berkeley, California') \
        == ['US-PR']
    assert get_countries('Hospital Kassel, Kassel, Germany') == ['DE']
    assert get_countries('New South Wales, Australia') == ['AU']
    assert get_countries('State of Illinois') == ['US-PR']
    assert get_countries('Lehigh Valley Hospital, Allentown, PA;') == ['US-PR']
    assert get_countries('Boston Children\'s Hospital, Boston, MA ') == \
        ['US-PR']
    assert get_countries('Winthrop University Hospital, Mineola, NY') == \
        ['US-PR']
    assert get_countries('Medical Dow Chemical Company, U.S.A.') == ['US-PR']
    assert get_countries('New York University') == ['US-PR']
    assert get_countries('Enschede, The Netherlands') == ['NL']
    assert get_countries('University of Surrey, Guildford, United Kingdom') \
        == ['GB-UK']
    assert get_countries('República Dominicana') == ['DO']
    assert get_countries('České Budějovice ,  Czech Republic') == ['CZ']
    assert get_countries('Washington, D.C.') == ['US-PR']
    assert get_countries('Agency for Health Care Policy and Research') == []
