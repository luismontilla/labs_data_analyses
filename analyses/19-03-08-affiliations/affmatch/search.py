import re

from affmatch.es_utils import es_search, get_query_phrase, \
    get_query_common, get_query_fuzzy, get_query_acronym
from affmatch.organization import MatchedOrganization
from affmatch.scoring import get_score
from affmatch.utils import normalize


def get_search_queries(text):
    yield text, get_query_phrase(normalize(text))
    yield text, get_query_common(normalize(text))
    yield text, get_query_fuzzy(normalize(text))
    for ac in re.findall('[A-Z]{3,}', text):
        yield ac, get_query_acronym(ac)


def search(text):
    all_matches = []
    for t, query in get_search_queries(text):
        candidates = es_search(query)
        if not candidates:
            continue
        for candidate in candidates:
            if candidate['id'] in [m.organization['id'] for m in all_matches]:
                continue
            score = get_score(candidate, t, None)
            all_matches.append(MatchedOrganization(text=t, score=score,
                                                   organization=candidate))
    all_matches = sorted(all_matches, key=lambda m: m.score, reverse=True)
    return all_matches
