import re

from affmatch.countries import to_region
from affmatch.utils import normalize
from fuzzywuzzy import fuzz


def get_similarity(aff_sub, cand_name):
    aff_sub = normalize(aff_sub)
    cand_name = normalize(cand_name)
    comparfun = fuzz.token_sort_ratio
    if '(' in aff_sub or ')' in aff_sub or '-' in aff_sub or \
        len([s for s in ['university', 'college', 'school', 'department',
                         'institute', 'center', 'hospital']
             if s in aff_sub]) > 1:
        comparfun = fuzz.partial_ratio
    cand_name = re.sub('\(.*\)', '', cand_name).strip()
    if min(len(aff_sub), len(cand_name)) < 12:
        return 100 if aff_sub == cand_name else 0
    return comparfun(aff_sub, cand_name)


def get_score(candidate, aff_sub, countries):
    if countries and \
            to_region(candidate['country']['country_code']) not in countries:
        return 0
    scores = [get_similarity(aff_sub, c) for c in
              [candidate['name']] +
              [l['label'] for l in candidate['labels']] +
              candidate['aliases']]
    if aff_sub != 'USA' and aff_sub in candidate['acronyms']:
        scores.append(100) if countries else scores.append(90)
    return max(scores)
