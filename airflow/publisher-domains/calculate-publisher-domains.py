import boto3
import json
import sys
import tldextract

from pyspark.sql import SparkSession


CONFIG = json.loads(sys.argv[1])

SAMPLES_BUCKET = CONFIG["samples-bucket"]
SAMPLES_DATE = CONFIG["samples-date"]
OUTPUT_BUCKET = CONFIG["output-bucket"]
OUTPUT_PREFIX = CONFIG["output-prefix"]

spark = SparkSession.builder.config(
    "spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.8.5"
).getOrCreate()


def get_member_id(member_path):
    return member_path.split("/")[-1][7:-6]


def get_domains(member_data):
    domains = {}
    lines = member_data.split("\n")
    for line in lines:
        item = json.loads(line)["data-point"]
        if "resource" not in item:
            continue
        url = item["resource"]["primary"]["URL"]
        if url is None or not url:
            continue
        domain = tldextract.extract(url).registered_domain
        if domain is None or not domain:
            continue
        if domain not in domains:
            domains[domain] = 0
        domains[domain] += 1

    return {"sample-size": len(lines), "domains": domains}


def save_member_domains(member, domains):
    total = domains["sample-size"]
    domains = domains["domains"]
    data = {
        "about": {
            "sample-collected-date": SAMPLES_DATE,
            "sample-size": total,
            "sample": f"members/{member}/works/samples/{SAMPLES_DATE}",
        },
        "domains": [
            {"value": k, "count": v, "fraction": v / total} for k, v in domains.items()
        ],
    }
    data["domains"].sort(key=lambda x: x["count"], reverse=True)

    session = boto3.Session()
    s3 = session.resource("s3")
    obj = s3.Object(OUTPUT_BUCKET, f"{OUTPUT_PREFIX}/{member}/domains.json")
    obj.put(Body=json.dumps(data))
    return 1


data = spark.sparkContext.wholeTextFiles(
    f"s3://{SAMPLES_BUCKET}/members-works/{SAMPLES_DATE}/samples/*.jsonl",
    minPartitions=1000,
)
data = data.map(lambda r: (get_member_id(r[0]), get_domains(r[1])))
saved = data.map(lambda r: save_member_domains(*r))
saved = saved.sum()
