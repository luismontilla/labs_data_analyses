from airflow.decorators import dag, task

from datetime import datetime
from datetime import timedelta

import boto3
import json
import base64
import re


def get_secret(secret_name):
    region_name = "us-east-1"

    session = boto3.session.Session()
    client = session.client(service_name="secretsmanager", region_name=region_name)

    get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    if "SecretString" in get_secret_value_response:
        return get_secret_value_response["SecretString"]
    else:
        return base64.b64decode(get_secret_value_response["SecretBinary"])


CODE_BUCKET = get_secret("bucket-name-emr-code")
BOOTSTRAP_SCRIPT = f"s3://{CODE_BUCKET}/publisher-domains/bootstrap.sh"
DOMAINS_SCRIPT = f"s3://{CODE_BUCKET}/publisher-domains/calculate-publisher-domains.py"

SAMPLES_BUCKET = get_secret("bucket-name-samples-data")
OUTPUT_BUCKET = get_secret("bucket-name-outputs")
OUTPUT_PREFIX = "annotations/members"


DEFAULT_ARGS = {
    "owner": "dtkaczyk",
    "depends_on_past": False,
    # "email": [get_secret("dtkaczyk-mailto")],
    "email_on_failure": False,
    "email_on_retry": False,
}


@dag(
    default_args=DEFAULT_ARGS,
    schedule_interval="@once",
    catchup=False,
    dagrun_timeout=timedelta(hours=10),
    start_date=datetime(2023, 4, 15),
    tags=["experiment", "domains"],
)
def publisher_domains():
    @task()
    def get_last_samples_date():
        s3_client = boto3.client("s3")
        response = s3_client.list_objects_v2(
            Bucket=SAMPLES_BUCKET, Prefix="members-works/", Delimiter="/"
        )
        last = ""
        for prefix in response["CommonPrefixes"]:
            s = re.search("\d{4}-\d{2}-\d{2}", prefix["Prefix"])
            date = s.group() if s else ""
            if date and date > last:
                last = date
        return last

    @task()
    def start_spark_app(last_samples_date: str):
        client = boto3.client("emr", region_name="us-east-1")
        response = client.run_job_flow(
            Name="publisher-domains",
            ReleaseLabel="emr-5.36.0",
            Applications=[{"Name": "Spark"}],
            Instances={
                "MasterInstanceType": "m5.xlarge",
                "SlaveInstanceType": "m5.xlarge",
                "InstanceCount": 5,
                "KeepJobFlowAliveWhenNoSteps": False,
                "TerminationProtected": False,
            },
            Steps=[
                {
                    "Name": "publisher-domains",
                    "ActionOnFailure": "TERMINATE_CLUSTER",
                    "HadoopJarStep": {
                        "Jar": "command-runner.jar",
                        "Args": [
                            "spark-submit",
                            "--deploy-mode",
                            "cluster",
                            DOMAINS_SCRIPT,
                            json.dumps(
                                {
                                    "samples-bucket": SAMPLES_BUCKET,
                                    "samples-date": last_samples_date,
                                    "output-bucket": OUTPUT_BUCKET,
                                    "output-prefix": OUTPUT_PREFIX,
                                }
                            ),
                        ],
                    },
                }
            ],
            BootstrapActions=[
                {
                    "Name": "Python packages",
                    "ScriptBootstrapAction": {"Path": BOOTSTRAP_SCRIPT},
                }
            ],
            LogUri=get_secret("s3-url-logs"),
            VisibleToAllUsers=True,
            ServiceRole="EMR_DefaultRole",
            JobFlowRole="EMR_EC2_DefaultRole",
        )
        return response

    @task()
    def wait_for_spark(spark_response: dict):
        client = boto3.client("emr", region_name="us-east-1")
        waiter = client.get_waiter("cluster_terminated")
        waiter.wait(
            ClusterId=spark_response["JobFlowId"],
            WaiterConfig={"Delay": 60, "MaxAttempts": 600},
        )

    last_samples_date = get_last_samples_date()
    spark_cluster = start_spark_app(last_samples_date)
    wait_for_spark(spark_cluster)


workflow = publisher_domains()
