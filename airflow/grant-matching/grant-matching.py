from airflow.decorators import dag, task
from botocore.exceptions import ClientError
from datetime import date
from datetime import datetime
from datetime import timedelta

import boto3
import json
import base64
import requests


def get_secret(secret_name):
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name='us-east-1'
    )

    get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    if 'SecretString' in get_secret_value_response:
        return get_secret_value_response['SecretString']
    else:
        return base64.b64decode(get_secret_value_response['SecretBinary'])


API_URL = 'https://api.crossref.org'
MAILTO = get_secret('grant-matching-mailto')

CODE_BUCKET = get_secret('bucket-name-emr-code')
BOOTSTRAP_SCRIPT = f's3://{CODE_BUCKET}/grant-matching/bootstrap.sh'
FUNDER_INFO_SCRIPT = f's3://{CODE_BUCKET}/grant-matching/download-funder-info.py'
MATCHING_SCRIPT = f's3://{CODE_BUCKET}/grant-matching/match-grants.py'

OUTPUT_PREFIX = f'grant-matching/{date.today()}'
OUTPUT_BUCKET = get_secret('bucket-name-outputs')
 
DEFAULT_ARGS = {
    'owner': 'dtkaczyk',
    'depends_on_past': False,
    'email': [get_secret('grant-matching-mailto')],
    'email_on_failure': False,
    'email_on_retry': False,
}


def start_spark_cluster(name, instance_count, script, script_config):
    client = boto3.client('emr', region_name='us-east-1')
    return client.run_job_flow(
        Name=name,
        ReleaseLabel='emr-5.36.0',
        Applications=[{
            'Name': 'Spark'
        }],
        Instances={
            'MasterInstanceType': 'm5.xlarge',
            'SlaveInstanceType': 'm5.xlarge',
            'InstanceCount': instance_count,
            'KeepJobFlowAliveWhenNoSteps': False,
            'TerminationProtected': False
        },
        Steps=[{
            'Name': name,
            'ActionOnFailure': 'TERMINATE_CLUSTER',
            'HadoopJarStep': {
                'Jar': 'command-runner.jar',
                'Args': ['spark-submit',
                         '--deploy-mode',
                         'cluster',
                         script,
                         json.dumps(script_config)
                ]
            }
        }],
        BootstrapActions=[{
            'Name': 'Python packages',
            'ScriptBootstrapAction': {
                'Path': BOOTSTRAP_SCRIPT
            }
        }],
        LogUri=get_secret('s3-url-logs'),
        VisibleToAllUsers=True,
        ServiceRole='EMR_DefaultRole',
        JobFlowRole='EMR_EC2_DefaultRole'
    )

def wait_for_spark_cluster(cluster):
    client = boto3.client('emr', region_name='us-east-1')
    waiter = client.get_waiter('cluster_terminated')
    waiter.wait(
        ClusterId=cluster['JobFlowId'],
        WaiterConfig={'Delay': 60, 'MaxAttempts': 1200}
    )


@dag(
    default_args=DEFAULT_ARGS,
    schedule_interval='@once',
    catchup=False,
    dagrun_timeout=timedelta(hours=20),
    start_date=datetime(2022, 12, 6),
    tags=['data analysis']
)
def grant_matching():

    @task()
    def get_funder_ids():
        funder_ids = []
        cursor = '*'
        while True:
            params = {'mailto': MAILTO, 'cursor': cursor, 'rows': 1000}
            data = requests.get(f'{API_URL}/funders', params).json()['message']
            items = data['items']
            cursor = data['next-cursor']
            if not items:
                break
            for item in items:
                funder_ids.append(f'10.13039/{item["id"]}')

        session = boto3.Session()
        s3 = session.resource('s3')
        output = f'{OUTPUT_PREFIX}/funder-ids.txt'
        obj = s3.Object(OUTPUT_BUCKET, output)
        obj.put(Body='\n'.join(funder_ids))
        return f's3://{OUTPUT_BUCKET}/{output}'

    @task()
    def start_spark_funders(funder_ids_file):
        output = f'{OUTPUT_PREFIX}/funders'
        script_config = {'funder-ids-file': funder_ids_file,
                         'output': f's3://{OUTPUT_BUCKET}/{output}',
                         'api-url': API_URL,
                         'mailto': MAILTO}
        print(output, script_config)
        return {'output': output, 'cluster': start_spark_cluster('funder-info', 9, FUNDER_INFO_SCRIPT, script_config)}

    @task()
    def wait_for_spark_funders(funders_cluster):
        wait_for_spark_cluster(funders_cluster['cluster'])
        return funders_cluster['output']

    @task()
    def get_funders(funders_dir):
        s3client = boto3.client('s3')
        paginator = s3client.get_paginator('list_objects_v2')
        funders_data = []
        for page in paginator.paginate(Bucket=OUTPUT_BUCKET, Prefix=funders_dir):
            for obj in page['Contents']:
                data = s3client.get_object(Bucket=OUTPUT_BUCKET, Key=obj['Key'])['Body'].read().decode('utf-8')
                funders_data.extend([json.loads(l.strip()) for l in data.split('\n') if l.strip()])

        funders = {funder['DOI']: funder for funder in funders_data}

        for funder in funders.values():
            for alias in funder['aliases']:
                if funder['DOI'] not in funders[alias]['aliases']:
                    funders[alias]['aliases'].append(funder['DOI'])

        for funder in funders.values():
            ans = []
            for alias in funder['aliases']:
                ans.extend(funders[alias]['names'])
            funder['aliases-names'] = list(set(ans))

        for funder in funders.values():
            dns = []
            for desc in funder['descendants']:
                dns.extend(funders[desc]['names'])
            funder['family-names'] = list(set(dns))

        for funder in funders.values():
            for desc in funder['descendants']:
                funders[desc]['family-names'].extend(funder['names'])
                funders[desc]['family-names'] = list(set(funders[desc]['family-names']))

        session = boto3.Session()
        s3 = session.resource('s3')
        output = f'{OUTPUT_PREFIX}/funders.json'
        obj = s3.Object(OUTPUT_BUCKET, output)
        obj.put(Body=json.dumps(funders))
        return f's3://{OUTPUT_BUCKET}/{output}'

    @task()
    def get_grants():
        session = boto3.Session()
        s3 = session.resource('s3')
        cursor = '*'
        i = 0
        while True:
            params = {'mailto': MAILTO, 'cursor': cursor, 'rows': 1000}
            data = requests.get(f'{API_URL}/types/grant/works', params).json()['message']
            items = data['items']
            cursor = data['next-cursor']
            if not items:
                break
            obj = s3.Object(OUTPUT_BUCKET, f'{OUTPUT_PREFIX}/grants/{i}.jsonl')
            obj.put(Body='\n'.join([json.dumps(g) for g in items]))
            i += 1
        return f's3://{OUTPUT_BUCKET}/{OUTPUT_PREFIX}/grants/*.jsonl'

    @task()
    def start_spark_app_matching(funders_file, grants_file):
        output = f'{OUTPUT_PREFIX}/links'
        script_config = {'funders-file': funders_file,
                         'grants-file': grants_file,
                         'output': f's3://{OUTPUT_BUCKET}/{output}',
                         'api-url': API_URL,
                         'mailto': MAILTO}
        return start_spark_cluster('grant-matching', 9, MATCHING_SCRIPT, script_config)

    @task()
    def wait_for_spark_matching(cluster):
        wait_for_spark_cluster(cluster)

    funder_ids_file = get_funder_ids()
    funders_cluster = start_spark_funders(funder_ids_file)
    funders_dir = wait_for_spark_funders(funders_cluster)
    funders_file = get_funders(funders_dir)
    grants_file = get_grants()
    matching_cluster = start_spark_app_matching(funders_file, grants_file)
    wait_for_spark_matching(matching_cluster)

workflow = grant_matching()

