import json
import re
import requests
import sys
import time

from pyspark.sql import SparkSession
from retry import retry


CONFIG = json.loads(sys.argv[1])
FUNDER_IDS_FILE = CONFIG['funder-ids-file']
OUTPUT = CONFIG['output']
API_URL = CONFIG['api-url']
MAILTO = CONFIG['mailto']


def funder_doi_norm(doi):
    return '10.13039/' + re.sub('.*\/', '', doi) if doi else ''

@retry(tries=5, delay=10)
def get_funder(funder_id):
    time.sleep(1)
    funder = requests.get(f'{API_URL}/funders/{funder_id}', {'mailto': MAILTO})
    funder = funder.json()['message']
    if 'id' not in funder:
        raise Exception('funder id missing')

    aliases = funder['replaces'] + funder['replaced-by']
    aliases = list(set([funder_doi_norm(a) for a in aliases]))
    names = [s.lower() for s in [funder['name']] + funder['alt-names']]
    descendants = [funder_doi_norm(d) for d in funder['descendants']]
    
    return {'DOI': funder_doi_norm(funder['id']),
            'aliases': aliases,
            'names': names,
            'descendants': descendants}


spark = SparkSession.builder \
            .config('spark.jars.packages', 'org.apache.hadoop:hadoop-aws:2.8.5') \
            .getOrCreate()

funder_ids = spark.sparkContext.textFile(FUNDER_IDS_FILE, minPartitions=1000)
funders = funder_ids.map(get_funder)
funders = funders.map(json.dumps)
funders.saveAsTextFile(OUTPUT)
