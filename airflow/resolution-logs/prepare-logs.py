import json
import re
import sys
import tldextract

from pyspark.sql import functions
from pyspark.sql import SparkSession

from urllib.parse import urlparse


CONFIG = json.loads(sys.argv[1])
INPUT = CONFIG["input"]
PREFIXES = CONFIG["prefixes"]
OUTPUT = CONFIG["output"]

spark = SparkSession.builder.config(
    "spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.8.5"
).getOrCreate()

logs = spark.read.option("mode", "DROPMALFORMED").option("delimiter", " ").csv(INPUT)
logs = logs.na.fill("").na.fill(0)
logs = logs.toDF(
    "ip",
    "method",
    "time",
    "operation",
    "status",
    "duration",
    "doi",
    "handle",
    "referrer",
)
logs = logs.drop("ip", "method", "time", "operation", "status", "duration")

prefixes = (
    spark.read.option("mode", "DROPMALFORMED").option("delimiter", " ").csv(PREFIXES)
)
prefixes = prefixes.toDF("member_prefix", "member")

extract_prefix = functions.udf(lambda x: re.sub(".*/", "", x))
logs = logs.withColumn("prefix", extract_prefix("handle"))
logs = logs.drop("handle")

logs = logs.join(prefixes, logs.prefix == prefixes.member_prefix, "inner")
logs = logs.drop("member_prefix")

extract_full_domain = functions.udf(lambda x: urlparse(x).netloc)
logs = logs.withColumn("full_domain", extract_full_domain("referrer"))

extract_registered_domain = functions.udf(
    lambda x: tldextract.extract(x).registered_domain
)
logs = logs.withColumn("registered_domain", extract_registered_domain("referrer"))

logs = logs.drop("referrer")

logs.write.format("csv").save(OUTPUT)
