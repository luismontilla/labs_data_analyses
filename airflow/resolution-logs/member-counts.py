import boto3
import json
import sys

from pyspark.sql import SparkSession
from pyspark.sql.functions import count


CONFIG = json.loads(sys.argv[1])
DATE = CONFIG["date"]
INPUT = CONFIG["input"]
OUTPUT_BUCKET = CONFIG["output-bucket"]
OUTPUT_PREFIX = CONFIG["output-prefix"]


def save_member_count(member, count):
    s3client = boto3.client("s3")
    key = f"{OUTPUT_PREFIX}/{member}/resolution.json"
    try:
        data = (
            s3client.get_object(Bucket=OUTPUT_BUCKET, Key=key)["Body"]
            .read()
            .decode("utf-8")
        )
        data = json.loads(data)
    except s3client.exceptions.NoSuchKey:
        data = []

    res_object = [o for o in data if o["about"]["logs-collected-date"] == DATE]
    if res_object:
        res_object = res_object[0]
    else:
        res_object = {"about": {"logs-collected-date": DATE}}
        data.append(res_object)
    res_object["total-count"] = count
    data.sort(key=lambda o: o["about"]["logs-collected-date"])
    data = data[-3:]

    session = boto3.Session()
    s3 = session.resource("s3")
    obj = s3.Object(OUTPUT_BUCKET, key)
    obj.put(Body=json.dumps(data))
    return 1


spark = SparkSession.builder.config(
    "spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.8.5"
).getOrCreate()

logs = spark.read.option("mode", "DROPMALFORMED").csv(INPUT)
logs = logs.na.fill("").na.fill(0)
logs = logs.toDF("doi", "prefix", "member", "full_domain", "registered_domain")

member_counts = logs.groupBy("member").agg(count("member").alias("count")).rdd
member_counts = member_counts.map(lambda x: save_member_count(x[0], x[1]))
member_counts = member_counts.sum()
