from airflow.decorators import dag, task

from datetime import datetime
from datetime import timedelta

from requests.auth import HTTPBasicAuth

import boto3
import json
import base64
import re
import requests


def get_secret(secret_name):
    region_name = "us-east-1"
    session = boto3.session.Session()
    client = session.client(service_name="secretsmanager", region_name=region_name)
    get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    if "SecretString" in get_secret_value_response:
        return get_secret_value_response["SecretString"]
    else:
        return base64.b64decode(get_secret_value_response["SecretBinary"])


LOGS_URL = "https://loganalysis.handle.net/crossref/"

BUCKET = get_secret("bucket-name-outputs-private")
PREFIX = "resolution-logs/"

CREDENTIALS_FILE = "credentials"
RAW_LOGS_PREFIX = "raw"
LOGS_FILE_PATTERN = "CrossRef-access.log*"
MEMBER_PREFIXES_FILE = "prefix-map.json"
NON_PUBLISHER_DOMAINS_FILE = f"s3://{BUCKET}/{PREFIX}non-publisher-domains.txt"
DOMAINS_PREFIX = "domains"
PREPROCESSED_PREFIX = "preprocessed"

API_URL = "https://api.crossref.org"
MAILTO = get_secret("sampling-framework-mailto")

CODE_BUCKET = get_secret("bucket-name-emr-code")
BOOTSTRAP_SCRIPT = f"s3://{CODE_BUCKET}/resolution-logs/bootstrap.sh"
LOGS_DOWNLOAD_SCRIPT = f"s3://{CODE_BUCKET}/resolution-logs/download-logs.py"
DOMAINS_SCRIPT = f"s3://{CODE_BUCKET}/resolution-logs/publisher-domains.py"
PREPROCESS_LOGS_SCRIPT = f"s3://{CODE_BUCKET}/resolution-logs/prepare-logs.py"
MEMBER_COUNTS_SCRIPT = f"s3://{CODE_BUCKET}/resolution-logs/member-counts.py"
MEMBER_BREAKDOWN_SCRIPT = f"s3://{CODE_BUCKET}/resolution-logs/member-breakdown.py"
OVERALL_STATISTICS_SCRIPT = f"s3://{CODE_BUCKET}/resolution-logs/overall-statistics.py"

SAMPLES_BUCKET = get_secret("bucket-name-samples-data")

OUTPUT_BUCKET = get_secret("bucket-name-outputs")
OUTPUT_PREFIX = "annotations/members"
OUTPUT_STATISTICS = "reports/overall/resolution.json"


DEFAULT_ARGS = {
    "owner": "dtkaczyk",
    "depends_on_past": False,
    "email": [get_secret("dtkaczyk-mailto")],
    "email_on_failure": False,
    "email_on_retry": False,
}


def step_config(name, script, args):
    return {
        "Name": name,
        "ActionOnFailure": "TERMINATE_CLUSTER",
        "HadoopJarStep": {
            "Jar": "command-runner.jar",
            "Args": [
                "spark-submit",
                "--deploy-mode",
                "cluster",
                script,
                json.dumps(args),
            ],
        },
    }


def breakdown_step_config(
    name, date, breakdown_field, output_field, exept=None, only=None
):
    return step_config(
        name,
        MEMBER_BREAKDOWN_SCRIPT,
        {
            "date": date,
            "input": f"s3://{BUCKET}/{PREFIX}{date}/{PREPROCESSED_PREFIX}",
            "output-bucket": OUTPUT_BUCKET,
            "output-prefix": OUTPUT_PREFIX,
            "breakdown-field": breakdown_field,
            "output-field": output_field,
            "except": exept,
            "only": only,
        },
    )


def start_spark(name, instance_type, instance_count, steps):
    client = boto3.client("emr", region_name="us-east-1")
    return client.run_job_flow(
        Name=name,
        ReleaseLabel="emr-5.36.0",
        Applications=[{"Name": "Spark"}],
        Instances={
            "MasterInstanceType": "m5.xlarge",
            "SlaveInstanceType": instance_type,
            "InstanceCount": instance_count,
            "KeepJobFlowAliveWhenNoSteps": False,
            "TerminationProtected": False,
        },
        Steps=steps,
        BootstrapActions=[
            {
                "Name": "Python packages",
                "ScriptBootstrapAction": {"Path": BOOTSTRAP_SCRIPT},
            }
        ],
        LogUri=get_secret("s3-url-logs"),
        VisibleToAllUsers=True,
        ServiceRole="EMR_DefaultRole",
        JobFlowRole="EMR_EC2_DefaultRole",
    )


@dag(
    default_args=DEFAULT_ARGS,
    schedule_interval="@once",
    catchup=False,
    dagrun_timeout=timedelta(hours=10),
    start_date=datetime(2023, 4, 15),
    tags=["data analysis"],
)
def resolution_logs():
    @task()
    def download_logs_summary():
        s3client = boto3.client("s3")
        credentials = (
            s3client.get_object(Bucket=BUCKET, Key=f"{PREFIX}{CREDENTIALS_FILE}")[
                "Body"
            ]
            .read()
            .decode("utf-8")
            .strip()
        )
        auth = HTTPBasicAuth(*credentials.split(":"))
        summary = requests.get(f"{LOGS_URL}/logs/config.json", auth=auth).json()
        date = summary["date"]
        summary["date"] = f"{date[:4]}-{date[-2:]}"
        return summary

    @task()
    def check_date(summary):
        date = summary["date"]
        s3 = boto3.resource("s3")
        if sum(1 for _ in s3.Bucket(BUCKET).objects.filter(Prefix=f"{PREFIX}{date}")):
            raise Exception(f"Already processed logs from {date}")
        return summary

    @task()
    def start_logs_download(summary):
        return start_spark(
            "resolution-logs-download",
            "m5.8xlarge",
            4,
            [
                step_config(
                    "resolution-logs-download",
                    LOGS_DOWNLOAD_SCRIPT,
                    {
                        "bucket": BUCKET,
                        "logs-prefix": f"{PREFIX}{summary['date']}/{RAW_LOGS_PREFIX}",
                        "logs-url": LOGS_URL,
                        "credentials-file": f"{PREFIX}{CREDENTIALS_FILE}",
                        "files": summary["files"],
                    },
                )
            ],
        )

    @task()
    def wait_for_logs_download(spark_response: dict):
        client = boto3.client("emr", region_name="us-east-1")
        waiter = client.get_waiter("cluster_terminated")
        waiter.wait(
            ClusterId=spark_response["JobFlowId"],
            WaiterConfig={"Delay": 60, "MaxAttempts": 600},
        )
        return True

    @task()
    def generate_prefix_map(summary):
        date = summary["date"]
        prefixes = []
        offset = 0
        while True:
            members = requests.get(
                f"{API_URL}/members", {"rows": 1000, "offset": offset}
            ).json()["message"]["items"]
            if not members:
                break
            for m in members:
                for p in m["prefixes"]:
                    prefixes.append(p + " " + str(m["id"]))
            offset += 1000

        session = boto3.Session()
        s3 = session.resource("s3")
        obj = s3.Object(BUCKET, f"{PREFIX}{date}/{MEMBER_PREFIXES_FILE}")
        obj.put(Body="\n".join(prefixes))
        return True

    @task()
    def get_last_samples_prefix():
        s3_client = boto3.client("s3")
        response = s3_client.list_objects_v2(
            Bucket=SAMPLES_BUCKET, Prefix="members-works/", Delimiter="/"
        )
        last = ""
        for prefix in response["CommonPrefixes"]:
            if (
                re.search("\d{4}-\d{2}-\d{2}", prefix["Prefix"])
                and prefix["Prefix"] > last
            ):
                last = prefix["Prefix"]
        return last

    @task()
    def start_publisher_domains(last_samples_prefix, summary):
        date = summary["date"]
        return start_spark(
            "resolution-logs-domains",
            "m5.xlarge",
            9,
            [
                step_config(
                    "resolution-logs-domains",
                    DOMAINS_SCRIPT,
                    {
                        "samples-bucket": SAMPLES_BUCKET,
                        "file-pattern": f"{last_samples_prefix}samples/*.jsonl",
                        "non-publisher-domains": NON_PUBLISHER_DOMAINS_FILE,
                        "output": f"s3://{BUCKET}/{PREFIX}{date}/{DOMAINS_PREFIX}",
                    },
                )
            ],
        )

    @task()
    def wait_for_publisher_domains(spark_response: dict):
        client = boto3.client("emr", region_name="us-east-1")
        waiter = client.get_waiter("cluster_terminated")
        waiter.wait(
            ClusterId=spark_response["JobFlowId"],
            WaiterConfig={"Delay": 60, "MaxAttempts": 600},
        )
        return True

    @task()
    def start_resolution_reports(
        summary, logs_downloaded, prefixes_saved, publisher_domains
    ):
        date = summary["date"]
        return start_spark(
            "resolution-reports",
            "m5.xlarge",
            9,
            [
                step_config(
                    "resolution-logs-preprocess",
                    PREPROCESS_LOGS_SCRIPT,
                    {
                        "input": f"s3://{BUCKET}/{PREFIX}{date}/{RAW_LOGS_PREFIX}/{LOGS_FILE_PATTERN}",
                        "prefixes": f"s3://{BUCKET}/{PREFIX}{date}/{MEMBER_PREFIXES_FILE}",
                        "output": f"s3://{BUCKET}/{PREFIX}{date}/{PREPROCESSED_PREFIX}",
                    },
                ),
                step_config(
                    "resolution-logs-overall-statistics",
                    OVERALL_STATISTICS_SCRIPT,
                    {
                        "date": date,
                        "input": f"s3://{BUCKET}/{PREFIX}{date}/{PREPROCESSED_PREFIX}",
                        "publisher-domains": f"s3://{BUCKET}/{PREFIX}{date}/{DOMAINS_PREFIX}",
                        "output-bucket": OUTPUT_BUCKET,
                        "output-file": OUTPUT_STATISTICS,
                    },
                ),
                step_config(
                    "resolution-logs-member-counts",
                    MEMBER_COUNTS_SCRIPT,
                    {
                        "date": date,
                        "input": f"s3://{BUCKET}/{PREFIX}{date}/{PREPROCESSED_PREFIX}",
                        "output-bucket": OUTPUT_BUCKET,
                        "output-prefix": OUTPUT_PREFIX,
                    },
                ),
                breakdown_step_config(
                    "resolution-logs-member-dois", date, "doi", "doi"
                ),
                breakdown_step_config(
                    "resolution-logs-member-full-domains",
                    date,
                    "full_domain",
                    "full-domain",
                ),
                breakdown_step_config(
                    "resolution-logs-member-publisher-domains",
                    date,
                    "registered_domain",
                    "publisher-domain",
                    only=f"s3://{BUCKET}/{PREFIX}{date}/{DOMAINS_PREFIX}",
                ),
                breakdown_step_config(
                    "resolution-logs-member-non-publisher-domains",
                    date,
                    "registered_domain",
                    "non-publisher-domain",
                    exept=f"s3://{BUCKET}/{PREFIX}{date}/{DOMAINS_PREFIX}",
                ),
            ],
        )

    @task()
    def wait_for_resolution_reports(spark_response: dict):
        client = boto3.client("emr", region_name="us-east-1")
        waiter = client.get_waiter("cluster_terminated")
        waiter.wait(
            ClusterId=spark_response["JobFlowId"],
            WaiterConfig={"Delay": 60, "MaxAttempts": 600},
        )
        return True

    logs_summary = download_logs_summary()
    summary = check_date(logs_summary)

    spark_cluster = start_logs_download(summary)
    logs_downloaded = wait_for_logs_download(spark_cluster)

    prefixes_saved = generate_prefix_map(summary)

    last_samples_prefix = get_last_samples_prefix()

    publisher_domains_cluster = start_publisher_domains(last_samples_prefix, summary)
    publisher_domains = wait_for_publisher_domains(publisher_domains_cluster)

    spark_cluster = start_resolution_reports(
        summary, logs_downloaded, prefixes_saved, publisher_domains
    )
    wait_for_resolution_reports(spark_cluster)


workflow = resolution_logs()
