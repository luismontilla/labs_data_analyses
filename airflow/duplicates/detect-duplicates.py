import json
import re
import requests
import sys
import urllib.parse
import time

from pyspark.sql import SparkSession
from retry import retry
from xml.etree import ElementTree


CONFIG = json.loads(sys.argv[1])
SAMPLE_INPUT = CONFIG['input']
OUTPUT = CONFIG['output']
API_URL = CONFIG['api-url']
MAILTO = CONFIG['mailto']


def is_suitable_for_analysis(item):
    if 'member' not in item or item.get('member') is None:
        return False
    title = item.get('title', [''])[0]
    tokens = re.split('[^a-zA-Z0-9]+', title)
    return len(title) >= 20 and len(tokens) >= 4

def to_query(item):
    parts = []
    for a in item.get('author', []):
        parts.append(a.get('family', ''))
    parts.append(' '.join(item.get('title', [])))
    parts.append(' '.join(item.get('container-title', [])))
    parts.append(' '.join(item.get('subtitle', [])))
    parts.append(' '.join(item.get('short-title', [])))
    parts.append(' '.join(item.get('original-title', [])))
    parts.append(' '.join(item.get('short-container-title', [])))

    parts.append(item.get('volume', ''))
    parts.append(item.get('issue', ''))
    parts.append(item.get('page', ''))
    parts.append(' '.join(item.get('ISSN', [])))
    
    if 'published-print' in item:
        parts.append(item.get('published-print').get('date-parts')[0][0])
    if 'issued' in item:
        parts.append(item.get('issued').get('date-parts')[0][0])

    query = ' '.join([str(t) for t in parts])
    return ' '.join(set(query.split()))

def get_xml(doi):
    doi = urllib.parse.quote(doi, safe='')
    r = requests.get(f'{API_URL}/works/{doi}/transform/application/vnd.crossref.unixsd+xml',
                     {'mailto': MAILTO})
    r.encoding = 'UTF-8'
    return ElementTree.fromstring(str(r.text))

def get_related(item):
    rels = item.findall(
        './/c:crm-item[@name="relation"]',
        namespaces={'c': 'http://www.crossref.org/qrschema/3.0'})
    return {r.text.lower(): r.attrib['claim'] for r in rels}

def get_alias(item):
    alias = item.findall(
        './/c:crm-item[@name="prime-doi"]',
        namespaces={'c': 'http://www.crossref.org/qrschema/3.0'})
    return None if not alias else alias[0].text

def are_duplicates(item1, item2):
    score_diff = abs(item1['score'] - item2['score'])
    if score_diff > 20:
        return False

    if item1['type'] != 'journal-article' or item2['type'] != 'journal-article':
        return False
    if not is_suitable_for_analysis(item1) or not is_suitable_for_analysis(item2):
        return False

    x1 = get_xml(item1['DOI'])
    x2 = get_xml(item2['DOI'])
    a1 = get_alias(x1)
    if a1 is not None and a1.lower() == item2['DOI'].lower():
        return False
    a2 = get_alias(x2)
    if a2 is not None and a2.lower() == item1['DOI'].lower():
        return False
    
    r1 = get_related(x1)
    if r1 and item2['DOI'].lower() in r1:
        return False
    r2 = get_related(x2)
    if r2 and item1['DOI'].lower() in r2:
        return False
    
    return True

def find_item_duplicates(item):
    query = to_query(item)
    results = requests.get(f'{API_URL}/works',
                           {'rows': 5,
                            'query.bibliographic': query,
                            'mailto': MAILTO})
    results = results.json().get('message', {}).get('items')
    
    me = [r for r in results if r['DOI'] == item['DOI']]
    if not me:
        return []
    me = me[0]
    
    duplicates = [(me, r) for r in results if r['DOI'] != item['DOI'] and are_duplicates(me, r)]
    if len(duplicates) > 2:
        return []
    
    return duplicates

@retry(tries=5, delay=10)
def find_duplicates(item):
    if not is_suitable_for_analysis(item):
        return []
    time.sleep(1)

    item_doi = item['DOI']
    item_member = item['member']

    duplicates = []
    candidates = find_item_duplicates(item)
    for item_copy, candidate in candidates:
        candidate_doi = candidate['DOI']
        candidate_member = candidate['member']
        diff = abs(item_copy['score'] - candidate['score'])

        candidates_rev = find_item_duplicates(candidate)
        candidate_rev = [d for d in candidates_rev if d[1]['DOI'] == item_doi]
        if candidate_rev:
            candidate_rev = candidate_rev[0]
            diff_rev = abs(candidate_rev[0]['score'] - candidate_rev[1]['score'])
            if diff > diff_rev:
                diff = diff_rev
        duplicates.append((item_doi, candidate_doi, diff, item_member, candidate_member))
 
    return [d for d in duplicates if d[2] <= 6]

def find_duplicates_retry(item):
    try:
        return find_duplicates(item)
    except:
        return []


spark = SparkSession.builder \
            .config('spark.jars.packages', 'org.apache.hadoop:hadoop-aws:2.8.5') \
            .getOrCreate()

data = spark.sparkContext.textFile(SAMPLE_INPUT, minPartitions=5000)
duplicates = data.flatMap(lambda d: find_duplicates(json.loads(d)['data-point']))
if not duplicates.isEmpty():
    duplicates = spark.createDataFrame(duplicates, 
                                       ['sampled-doi', 'duplicate-doi', 'score-difference',
                                        'sampled-member', 'duplicate-member'])
    duplicates.write.mode('overwrite').format('csv').save(OUTPUT)
